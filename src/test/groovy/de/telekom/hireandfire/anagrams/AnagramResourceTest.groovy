package de.telekom.hireandfire.anagrams

import de.telekom.interviewexercise.InterviewExerciseApplication
import de.telekom.interviewexercise.hireandfire.anagrams.AnagramCheckResponse
import de.telekom.interviewexercise.hireandfire.anagrams.AnagramsResponse
import de.telekom.interviewexercise.hireandfire.anagrams.AnagramResource
import de.telekom.interviewexercise.hireandfire.randomstuff.EntityRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = InterviewExerciseApplication)
@ActiveProfiles("test")
class AnagramResourceTest extends Specification {
    @Autowired(required = false)
    private AnagramResource restController

    @Autowired
    private EntityRepo repo

    def "all expected beans are created"() {
        expect: "the REST-Controller is created"
        restController
    }

    def "The given strings are not anagrams"(){
        // TODO implement this test
        when:
        true
        then:
        false
    }

    def "The given strings are anagrams within the validation boundaries"(){
        when:
        true
        // TODO implement this test
        then:
        false
    }

    def "The first of The given strings is two long"(){
        expect: "An exception is thrown"
        when:
        AnagramCheckResponse  response = restController.checkStrings("Abcdefghijklmnop", "sett")
        then:
        thrown(Exception.class)
    }

    def "The second of The given strings is two long"(){
        expect: "An exception is thrown"
        when:
        AnagramCheckResponse  response = restController.checkStrings("sett", "Abcdefghijklmnop")
        then:
        thrown(Exception.class)
    }

    def "When generating anagrams, the given string contains numbers"(){
        expect: "An exception is thrown"
        when:
        restController.fetchAnagramsForString("set245t")
        then:
        thrown(Exception.class)
    }

    def "Generate anagrams with valid input"(){
        expect: "Generated list of anagrams"
        when:
        AnagramsResponse response = restController.fetchAnagramsForString("FEUER")
        then:
        return
        // TODO: implement the check - remove the return stmt
    }
}
