package de.telekom.interviewexercise.hireandfire.anagrams;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Set;

@RestController
@CrossOrigin
@Validated
@RequestMapping("/v1/anagrams")
@Api(tags = "A simple endpoint to manage strings and their anagrams")
public class AnagramResource
{
    @Autowired private AnagramChecker anagramChecker;
    @Autowired private AnagramDao anagramDao;

    static final String VALID_REGEX = "^[\\p{Alpha}]+$";

    // TODO: annotate controller method and parameters
    public AnagramsResponse fetchAnagramsForString(
            String input,
            Boolean persist)
    {
        Set<String> result;
        if (anagramDao.anagramExists(input)) {
            result = anagramDao.retrieveAnagrams(input);
        }
        else {
            result = anagramChecker.generateAnagrams(input);
            if (persist) {
                anagramDao.persistAnagrams(result);
            }
        }
        return new AnagramsResponse(result);
    }

    @ApiOperation("Deletes all anagrams for the given string")
    @ApiResponse(code = 200, message = "OK", response = AnagramsResponse.class)
    @DeleteMapping(value = "/{input}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteAnagramsForString(
            @ApiParam(value = "An input string to delete the anagrams for", name = "input", required = true, type = "string", example = "SomeText")
            @PathVariable("input")
            @NotBlank(message = "Must not be empty") @Size(max = 10, min = 2, message = "Must be between 2 and 10 characters long") @Pattern(regexp = VALID_REGEX, message = "Must only contain alphabetic characters") String input)
    {
        if (anagramDao.anagramExists(input)) {
            anagramDao.deleteAnagram(input);
        }
    }
    // TODO: add a controller method to check 2 strings if they are anagrams of each other.
}
