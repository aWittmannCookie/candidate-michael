package de.telekom.interviewexercise.hireandfire.anagrams;

import org.springframework.stereotype.Service;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Service
public class SimpleAnagramChecker implements AnagramChecker
{
    @Override
    public boolean isAnagram(
            String first,
            String second)
    {
        if (first == null || second == null || first.isEmpty() || second.isEmpty()){
            return false;
        }
        return Arrays.equals(generateKey(first),generateKey(second));
    }

    @Override
    public char[] generateKey(String input){
        // TODO: implement this method.
        return null;
    }


    @Override
    public Set<String> generateAnagrams(@NotNull @NotEmpty String str) {
        Set<String > result = new HashSet<>();
        // TODO: implement this method.
        return result;
    }

}
