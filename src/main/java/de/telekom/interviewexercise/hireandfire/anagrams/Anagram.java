package de.telekom.interviewexercise.hireandfire.anagrams;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Anagram
{
    String key;
    String value;

    public Anagram(
            String key,
            String value)
    {
        this.key = key;
        this.value = value;
    }
}
