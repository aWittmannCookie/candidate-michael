package de.telekom.interviewexercise.hireandfire.anagrams;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class AnagramCheckResponse
{
    public AnagramCheckResponse(
            Boolean anagram,
            Set<String> anagrams)
    {
        super();
        this.anagram = anagram;
        this.anagrams = anagrams == null ? new HashSet<>() : anagrams;
    }

    Boolean anagram = false;
    Set<String> anagrams;
}
