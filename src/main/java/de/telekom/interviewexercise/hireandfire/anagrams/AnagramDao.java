package de.telekom.interviewexercise.hireandfire.anagrams;

import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Set;

@Repository
public class AnagramDao
{
    public Boolean anagramExists(String input)
    {
        // TODO: implement this method
        return false;
    }

    public void persistAnagrams(Set<String> anagrams)
    {
        // TODO: implement this method
    }

    public void deleteAnagram(String text)
    {
        // TODO: implement this method
    }

    public Set<String> retrieveAnagrams(String text)
    {
        Set<String> result = new HashSet<>();
        // TODO: implement this method
        return result;
    }
}
